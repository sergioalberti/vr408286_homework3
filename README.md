# Homework 3 - Sergio Alberti VR408286

## Punto 2

Processare la rosbag *V1_01_easy.bag* attraverso la libreria ORB SLAM 2 richiede l'esecuzione dei seguenti comandi (in tre terminali diversi) come riportato nella documentazione della stessa:

    $ roscore
    $ rosrun ORB_SLAM2 Stereo Vocabulary/ORBvoc.txt Examples/Stereo/EuRoC.yaml true
    $ rosbag play --pause V1_01_easy.bag /cam0/image_raw:=/camera/left/image_raw /cam1/image_raw:=/camera/right/image_raw

Il risultato è una mappa sparsa (di conseguenza la point cloud da generare sarà piuttosto leggera). L'immagine sottostante mostra la libreria ORB SLAM 2 in esecuzione.

![orbslam2](img/orbslam2.png)

## Punto 3

Modificare la libreria per poter generare un file PCD ha richiesto di lavorare sul file *src/System.cc* in cui è stata inserita la funzione `savePointCloud()` e aggiungerne il prototipo nell'header *include/System.hh*

```cpp
    //Saves the PointCloud as pcd
    void SavePointCloud(const string &file);
```
Sostanzialmente la funzione si occupa di ottenere tutti i punti della mappa generata dall'algoritmo ORB SLAM e creare un file compatibile con il formato *Point Cloud Data v7*. Sinteticamente:

* estrae tutti i punti dalla mappa con `GetAllMapPoints()`
* crea coerentemente (è necessario conoscere il numero di punti) l'header del file PCD
* per ogni punto estratto, ne ottiene le coordinate con `GetWorldPos()` e le scrive sul file PCD

E' infine stato modificato l'eseguibile dell'esempio *ros_stereo.cc* per inserire la chiamata alla funzione appena aggiunta una volta terminata l'esplorazione dello spazio.

**Esecuzione**

Per eseguire la versione di ORB SLAM 2 modificata è necessario **rimpiazzare i file originali** con quelli presenti nella directory *punto3/ORB_SLAM* e ricompilare il tutto utilizzando i due script:

    ./build.sh
    ./build_ros.sh

Il file PCD ottenuto si chiama `V1_01_easy.pcd` ed è situato nella root *ORB_SLAM2*.

**NOTE**

Sul mio pc, la libreria originale di ORB SLAM 2, una volta terminata la creazione della mappa entra in uno stato di lock e non permette di portare a termine l'esecuzione del programma (quindi non genera la point cloud).

Per risolvere in maniera piuttosto sbrigativa (sarebbe necessario un workaround serio), ho inserito un ciclo while nell'eseguibile:

```cpp
    while( ros::ok() ){
        ros::spin();
    }
```

In questo modo, una volta terminata la creazione della mappa, è sufficiente uccidere prima il processo `roscore` e poi uccidere il processo relativo a `rosrun ORB_SLAM2 ...` utilizzando in entrambi i casi `Ctrl+C` in terminale per portare a termine l'esecuzione e generare correttamente il file PCD.

## Punto 4

Questo punto consiste in un semplice programma C++ che clusterizza i punti della point cloud generata nel punto precedente per poi visualizzarli.

La clusterizzazione viene gestita attraverso la distanza Euclidea. Si è scelto di utilizzare come valore di tolleranza 10cm e un numero di punti per cluster minimo pari a 30. Non sono stati assegnati altri parametri rilevanti in quanto il risultato appare sufficiente.

```cpp
cluster_extr.setClusterTolerance(0.1);  //10cm
cluster_extr.setMinClusterSize(30);     //min 30 points per cluster
```
Si noti che essendo la mappa iniziale sparsa e comunque piuttosto leggera e contenuta si è scelto di non effettuare alcun *downsample*.

Il risultato finale è quello presente nella seguente immagine:

![clustering_result](img/clustering.png)

**Compilazione**

	mkdir build
	cd build
	cmake ..
	make

**Esecuzione**

	cd bin
	./vr408286_homework3

**NOTA**

Per la corretta esecuzione dev'essere presente il file `V1_01_easy.pcd` nella directory *bin/*. In caso contrario lo si può generare tramite il metodo descritto nel Punto 3.
