#include <pcl/io/pcd_io.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <sstream>

int main(int argc, char **argv)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr in_cloud(new pcl::PointCloud<pcl::PointXYZ> ());
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
    pcl::EuclideanClusterExtraction<pcl::PointXYZ> cluster_extr;
    std::vector<pcl::PointIndices> cluster_indices;

    if ( pcl::io::loadPCDFile( "V1_01_easy.pcd", *in_cloud ) < 0) {
        std::cout << "Error loading the pcd file." << std::endl;
        return -1;
    }

    //----- Divide the point cloud into clusters -----------------------------//

    // SET EXTRACTION PARAMETERS
    cluster_extr.setClusterTolerance(0.1);  //set to 10cm
    cluster_extr.setMinClusterSize(30);     //min 30 points per cluster

    // KD tree for point neighbours
    tree->setInputCloud(in_cloud);
    cluster_extr.setSearchMethod(tree);
    cluster_extr.setInputCloud(in_cloud);

    // extract the clusters
    cluster_extr.extract(cluster_indices);

    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("V1_01_easy PCD viewer"));
    viewer->setBackgroundColor(0, 0, 0);
    viewer->addCoordinateSystem(1.0);
    viewer->initCameraParameters();

    std::string base_name("Cluster");
    int j = 0;

    //for each cluster
    for(std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin(); it != cluster_indices.end(); ++it){

        // create a cloud for each cluster
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster(new pcl::PointCloud<pcl::PointXYZ>);

        //for each point in the cluster
        for(std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); pit++)
            cloud_cluster->points.push_back(in_cloud->points[*pit]);

        cloud_cluster->width = cloud_cluster->points.size();
        cloud_cluster->height = 1;
        cloud_cluster->is_dense = true;

        std::stringstream sbuf;
        sbuf<<base_name;
        sbuf<<j;

        pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZ> cluster_color(cloud_cluster, rand()%256, rand()%256, rand()%256);

        //add the cluster to the viewer
        viewer->addPointCloud<pcl::PointXYZ>(cloud_cluster, cluster_color, sbuf.str());
        j++;
    }

    while (!viewer->wasStopped ()){
        viewer->spinOnce ( 1 );
    }
}
